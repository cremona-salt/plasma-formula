# frozen_string_literal: true

# Overide by OS
package_name = 'plasma-desktop'
# package_name = 'cronie' if (os[:name] == 'centos') && os[:release].start_with?('6')

control 'plasma package' do
  title 'should not be installed'

  describe package(package_name) do
    it { should_not be_installed }
  end
end

