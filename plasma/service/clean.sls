# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import plasma with context %}

plasma-service-clean-service-dead:
  service.dead:
    - name: {{ plasma.service.name }}
    - enable: False
