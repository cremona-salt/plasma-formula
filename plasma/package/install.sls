# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import plasma with context %}

plasma-package-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ plasma.pkgs | yaml }}
